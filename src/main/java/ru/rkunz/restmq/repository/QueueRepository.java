package ru.rkunz.restmq.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.rkunz.restmq.model.Queue;

import java.util.List;

/**
 * Created by rkunz on 31.03.2017.
 */
@Repository
public interface QueueRepository extends CrudRepository<Queue, String>{

    @Override
    List<Queue> findAll();
}
