package ru.rkunz.restmq.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.rkunz.restmq.model.Message;
import ru.rkunz.restmq.model.Queue;

import java.util.UUID;

/**
 * Created by rkunz on 31.03.2017.
 */
@Repository
public interface MessageRepository extends CrudRepository<Message, UUID> {

    long countByQueue(Queue queue);

    Message findFirstByQueueOrderByCreated(Queue queue);
}
