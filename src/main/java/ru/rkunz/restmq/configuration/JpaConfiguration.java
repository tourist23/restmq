package ru.rkunz.restmq.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * Created by rkunz on 31.03.2017.
 */
@Configuration
@EnableJpaAuditing
public class JpaConfiguration {

}
