package ru.rkunz.restmq.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Objects;

/**
 * Created by rkunz on 31.03.2017.
 */
@Entity
@Table(name = "queues")
public class Queue {

    @Id
    @Column(length = 50)
    @NotNull(message = "queue.name.notnull")
    @Size(min = 1, max = 50, message = "queue.name.size")
    @Pattern(regexp = "^[a-zA-Z0-9]+$", message = "queue.name.format")
    private String name;

    public Queue() {
    }

    public Queue(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Queue queue = (Queue) o;

        return name.equals(queue.name);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.name);
        return hash;

    }

    @Override
    public String toString() {
        return "Queue{" +
                "name='" + name + '\'' +
                '}';
    }
}
