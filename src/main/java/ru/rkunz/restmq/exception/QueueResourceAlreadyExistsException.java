package ru.rkunz.restmq.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by rkunz on 01.04.2017.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class QueueResourceAlreadyExistsException extends RuntimeException {

    public QueueResourceAlreadyExistsException(String queueName) {
        super(String.format("Queue with name = %s already exists", queueName));
    }
}
