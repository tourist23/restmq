package ru.rkunz.restmq.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;

/**
 * Created by rkunz on 01.04.2017.
 */
public class QueueResourceNotFoundException extends ResourceNotFoundException {

    public QueueResourceNotFoundException(String queueName) {
        super(String.format("Queue with name = %s is not found", queueName));
    }
}
