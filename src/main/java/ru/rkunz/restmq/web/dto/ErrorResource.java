package ru.rkunz.restmq.web.dto;

import org.springframework.http.HttpStatus;

import java.util.List;

/**
 * Created by rkunz on 31.03.2017.
 */
public class ErrorResource {

    private HttpStatus status;

    private String message;

    private List<String> errors;

    public ErrorResource() {
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "ErrorResource{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", errors=" + errors +
                '}';
    }
}
