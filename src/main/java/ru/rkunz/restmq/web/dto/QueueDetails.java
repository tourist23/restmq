package ru.rkunz.restmq.web.dto;

/**
 * Created by rkunz on 01.04.2017.
 */
public class QueueDetails {

    private long size;

    public QueueDetails() {
    }

    public QueueDetails(long size) {
        this.size = size;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}
