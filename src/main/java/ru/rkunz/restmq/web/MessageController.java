package ru.rkunz.restmq.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import ru.rkunz.restmq.model.Message;
import ru.rkunz.restmq.services.MessageService;

import javax.validation.Valid;
import java.util.*;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * Created by rkunz on 31.03.2017.
 */

@RestController
@RequestMapping("/api/queues/{queueName}/messages")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @GetMapping
    public ResponseEntity<Message> getMessage(
            @PathVariable("queueName") String queueName, WebRequest request){

        Message message = this.messageService.getNextMessage(queueName);
        if(message == null){
            return ResponseEntity.noContent().build();
        }

        if(request.checkNotModified(message.getMessageId().toString())){
            return ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .eTag(message.getMessageId().toString())
                .body(message);
    }

    @PostMapping
    public ResponseEntity<Message> create(@PathVariable("queueName") String queueName,
                          @Valid @RequestBody Message message){

        Message createdMessage = this.messageService.create(queueName, message);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .eTag(createdMessage.getMessageId().toString())
                .body(createdMessage);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable UUID id){

        this.messageService.delete(id);
    }

}
