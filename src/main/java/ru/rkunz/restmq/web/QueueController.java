package ru.rkunz.restmq.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.rkunz.restmq.model.Queue;
import ru.rkunz.restmq.services.QueueService;
import ru.rkunz.restmq.web.dto.QueueDetails;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by rkunz on 31.03.2017.
 */
@RestController
@RequestMapping("/api/queues")
public class QueueController {

    @Autowired
    private QueueService queueService;

    @GetMapping
    public List<Queue> getQueues(){
        return this.queueService.getAllQueues();
    }

    @GetMapping("/{name}")
    public Queue getQueue(@PathVariable("name") String name){
        return this.queueService.getQueue(name);
    }

    @GetMapping(path = "/{name}", params = "details=true")
    @ResponseStatus(HttpStatus.OK)
    public QueueDetails getCountMessages(@PathVariable("name") String name){
        QueueDetails queueDetails = new QueueDetails();
        queueDetails.setSize(this.queueService.getSize(name));
        return queueDetails;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Queue create(@Valid @RequestBody Queue queue){
        return this.queueService.create(queue);
    }
}
