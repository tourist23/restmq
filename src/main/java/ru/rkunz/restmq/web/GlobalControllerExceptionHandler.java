package ru.rkunz.restmq.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.rkunz.restmq.web.dto.ErrorResource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by rkunz on 31.03.2017.
 */
@ControllerAdvice
public class GlobalControllerExceptionHandler extends ResponseEntityExceptionHandler{

    private static final Logger LOGGER
            = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);

    @Autowired
    private MessageSource messageSource;

    @ResponseStatus(value=HttpStatus.CONFLICT,
            reason="Data integrity violation")
    @ExceptionHandler(DataIntegrityViolationException.class)
    public void handleCoflict(DataIntegrityViolationException ex){

        LOGGER.info("DataIntegrityViolationException occured: " + ex.getMessage());
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR,
            reason = "Database error")
    @ExceptionHandler(SQLException.class)
    public void handleDatabaseError(SQLException ex){

        LOGGER.info("SQLException occured: " + ex.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {

        LOGGER.info("MethodArgumentNotValidException occured: " + ex.getMessage());

        ErrorResource errorResource = new ErrorResource();
        errorResource.setStatus(HttpStatus.BAD_REQUEST);
        errorResource.setMessage(ex.getLocalizedMessage());
        errorResource.setErrors(getAllErrors(ex));

        return super.handleExceptionInternal(
                ex, errorResource, headers, HttpStatus.BAD_REQUEST, request);
    }

    private List<String> getAllErrors(MethodArgumentNotValidException ex){
        Locale currentLocale = LocaleContextHolder.getLocale();
        List<String> errors = new ArrayList<String>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + getLocalizedMessage(error, currentLocale));
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {

            errors.add(error.getObjectName() + ": " + getLocalizedMessage(error, currentLocale));
        }
        return errors;
    }

    private String getLocalizedMessage(ObjectError error, Locale currentLocale){
        String localizedMessage
                = this.messageSource.getMessage(
                error.getDefaultMessage(), null, currentLocale);
        return localizedMessage;
    }
}
