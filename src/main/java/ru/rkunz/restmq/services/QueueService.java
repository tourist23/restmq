package ru.rkunz.restmq.services;

import ru.rkunz.restmq.model.Queue;

import java.util.List;

/**
 * Created by rkunz on 31.03.2017.
 */
public interface QueueService {

    Queue getQueue(String queueName);

    Queue create(Queue queue);

    long getSize(String queueName);

    List<Queue> getAllQueues();
}
