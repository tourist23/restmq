package ru.rkunz.restmq.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import ru.rkunz.restmq.exception.QueueResourceAlreadyExistsException;
import ru.rkunz.restmq.exception.QueueResourceNotFoundException;
import ru.rkunz.restmq.model.Queue;
import ru.rkunz.restmq.repository.MessageRepository;
import ru.rkunz.restmq.repository.QueueRepository;

import java.util.List;
import java.util.Objects;

/**
 * Created by rkunz on 31.03.2017.
 */
@Service
public class QueueServiceImpl implements QueueService{

    private static final Logger LOGGER
            = LoggerFactory.getLogger(QueueServiceImpl.class);

    @Autowired
    private QueueRepository queueRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Override
    public Queue getQueue(String queueName) {
        Objects.requireNonNull(queueName, "Queue name is null");
        return findAndCheck(queueName);
    }

    private Queue findAndCheck(String queueName){

        LOGGER.debug("Getting queue by name = {}", queueName);;
        Queue queue = this.queueRepository.findOne(queueName);
        if(queue == null){
            throw new QueueResourceNotFoundException(queueName);
        }

        LOGGER.debug("Found queue with name = {}", queueName);
        return queue;
    }

    @Override
    @Transactional
    public Queue create(Queue queue) {
        Objects.requireNonNull(queue, "Queue is null");

        LOGGER.debug("Creating a new queue");
        if(exists(queue)){
            throw new QueueResourceAlreadyExistsException(queue.getName());
        }

        Queue newQueue = new Queue();
        newQueue.setName(queue.getName());

        Queue createdQueue = this.queueRepository.save(newQueue);
        LOGGER.debug("Created a queue: {}", createdQueue);

        return createdQueue;
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long getSize(String queueName) {
        Objects.requireNonNull(queueName, "Queue name is null");

        LOGGER.debug("Getting the queue size {}", queueName);

        Queue queue = findAndCheck(queueName);

        long size = this.messageRepository.countByQueue(queue);

        LOGGER.debug("The queue size {} = {}", queueName, size);

        return size;
    }

    @Override
    public List<Queue> getAllQueues() {
        LOGGER.debug("Getting all queues");

        return this.queueRepository.findAll();
    }

    private boolean exists(Queue queue){
        return this.queueRepository.exists(queue.getName());
    }
}
