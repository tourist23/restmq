package ru.rkunz.restmq.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import ru.rkunz.restmq.model.Message;
import ru.rkunz.restmq.model.Queue;
import ru.rkunz.restmq.repository.MessageRepository;

import java.util.Objects;
import java.util.UUID;

/**
 * Created by rkunz on 31.03.2017.
 */
@Service
public class MessageServiceImpl implements MessageService{

    private static final Logger LOGGER
            = LoggerFactory.getLogger(MessageServiceImpl.class);

    @Autowired
    private MessageRepository messageRepository;

    @Autowired QueueService queueService;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Message getNextMessage(String queueName) {
        Objects.requireNonNull(queueName, "Queue name is null");

        LOGGER.debug("Getting the next message for queue = {}", queueName);

        Queue queue = this.queueService.getQueue(queueName);

        Message message = this.messageRepository.findFirstByQueueOrderByCreated(queue);

        LOGGER.debug("Found the message: {}", message);

        return message;
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Message create(String queueName, Message message) {
        Objects.requireNonNull(message, "Message is null");
        Objects.requireNonNull(queueName, "Queue name is null");

        LOGGER.debug("Creating a new message");

        Queue queue = this.queueService.getQueue(queueName);

        Message newMessage = new Message(message.getContent());
        newMessage.setQueue(queue);

        Message createdMessage = this.messageRepository.save(newMessage);
        LOGGER.debug("Created a message: {}", createdMessage);

        return createdMessage;
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void delete(UUID id) {
        Objects.requireNonNull(id, "Id is null");

        LOGGER.debug("Deleting message with id = {}", id);

        this.messageRepository.delete(id);

        LOGGER.debug("Message with id = {} id deleted", id);
    }
}
