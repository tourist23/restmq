package ru.rkunz.restmq.services;

import ru.rkunz.restmq.model.Message;
import ru.rkunz.restmq.model.Queue;

import java.util.UUID;

/**
 * Created by rkunz on 31.03.2017.
 */
public interface MessageService {

    Message getNextMessage(String queueName);

    Message create(String queueName, Message message);

    void delete(UUID id);
}
