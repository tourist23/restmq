package ru.rkunz.restmq.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.rkunz.restmq.exception.QueueResourceAlreadyExistsException;
import ru.rkunz.restmq.exception.QueueResourceNotFoundException;
import ru.rkunz.restmq.model.Queue;
import ru.rkunz.restmq.services.QueueService;
import ru.rkunz.restmq.web.dto.QueueDetails;

import java.io.IOException;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

/**
 * Created by rkunz on 01.04.2017.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(QueueController.class)
public class QueueControllerTest extends BaseControllerTest{

    private static final String QUEUE_NAME = "test";
    private static final Long QUEUE_SIZE = 10L;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private QueueService queueService;

    @Test
    public void getQueuesShouldReturnQueueDetails() throws Exception {
        System.out.println("getQueuesShouldReturnQueueDetails");

        given(this.queueService.getSize(QUEUE_NAME)).willReturn(QUEUE_SIZE);

        this.mvc.perform(get("/api/queues/test?details=true"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size", is(QUEUE_SIZE.intValue())));
    }

    @Test
    public void getQueuesShouldReturnError() throws Exception {
        System.out.println("getQueuesShouldReturnError");

        given(this.queueService.getSize(QUEUE_NAME))
                .willThrow(new QueueResourceNotFoundException(QUEUE_NAME));

        this.mvc.perform(get("/api/queues/test?details=true"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createShouldReturnQueue() throws Exception {
        System.out.println("createShouldReturnQueue");

        given(this.queueService.create(new Queue(QUEUE_NAME)))
                .willReturn(new Queue(QUEUE_NAME));

        this.mvc.perform(post("/api/queues")
                .content(this.json(new Queue(QUEUE_NAME)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(QUEUE_NAME)));
    }

    @Test
    public void createShouldReturnError() throws Exception {
        System.out.println("createShouldReturnError");

        given(this.queueService.create(new Queue(QUEUE_NAME)))
                .willThrow(new QueueResourceAlreadyExistsException(QUEUE_NAME));

        this.mvc.perform(post("/api/queues")
                .content(this.json(new Queue(QUEUE_NAME)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }


}
