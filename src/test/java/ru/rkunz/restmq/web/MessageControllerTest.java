package ru.rkunz.restmq.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.rkunz.restmq.exception.QueueResourceAlreadyExistsException;
import ru.rkunz.restmq.model.Message;
import ru.rkunz.restmq.model.Queue;
import ru.rkunz.restmq.services.MessageService;

import java.util.Date;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

/**
 * Created by rkunz on 01.04.2017.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(MessageController.class)
public class MessageControllerTest extends BaseControllerTest{

    private static final String QUEUE_NAME = "test";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MessageService messageService;

    @Autowired
    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Test
    public void getMessageShouldReturnNull() throws Exception {
        System.out.println("getNextMessageShouldReturnNull");

        given(this.messageService.getNextMessage(QUEUE_NAME)).willReturn(null);

        this.mvc.perform(get("/api/queues/test/messages"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void getMessageShouldReturnMessage() throws Exception {
        System.out.println("getNextMessageShouldReturnMessage");

        Message message = new Message();
        message.setMessageId(UUID.randomUUID());
        message.setContent("OpenSuse");
        message.setQueue(new Queue(QUEUE_NAME));
        message.setCreated(new Date());

        given(this.messageService.getNextMessage(QUEUE_NAME)).willReturn(message);

        this.mvc.perform(get("/api/queues/test/messages"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.messageId", is(message.getMessageId().toString())))
                .andExpect(jsonPath("$.content", is(message.getContent())))
                .andExpect(jsonPath("$.created", is(message.getCreated().getTime())))
                .andExpect(jsonPath("$.queue.name", is(message.getQueue().getName())));

        this.mvc.perform(get("/api/queues/test/messages")
                .header("If-None-Match", etag(message)))
                .andExpect(status().isNotModified())
                .andExpect(header().string("ETag", etag(message)));
    }

    @Test
    public void createShouldReturnError() throws Exception {
        System.out.println("createShouldReturnError");

        Message message = new Message();
        message.setMessageId(UUID.randomUUID());
        message.setContent("OpenSuse");
        message.setQueue(new Queue(QUEUE_NAME));
        message.setCreated(new Date());


        given(this.messageService.create(QUEUE_NAME, new Message("OpenSuse")))
                .willThrow(new QueueResourceAlreadyExistsException(QUEUE_NAME));

        this.mvc.perform(post("/api/queues/test/messages")
                .content(this.json(new Message("OpenSuse")))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createShouldReturnMessage() throws Exception {
        System.out.println("createShouldReturnMessage");

        Message message = new Message();
        message.setMessageId(UUID.randomUUID());
        message.setContent("OpenSuse");
        message.setQueue(new Queue(QUEUE_NAME));
        message.setCreated(new Date());


        given(this.messageService.create(QUEUE_NAME, new Message("OpenSuse")))
                .willReturn(message);

        this.mvc.perform(post("/api/queues/test/messages")
                .content(this.json(new Message("OpenSuse")))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.messageId", is(message.getMessageId().toString())))
                .andExpect(jsonPath("$.content", is(message.getContent())))
                .andExpect(jsonPath("$.created", is(message.getCreated().getTime())))
                .andExpect(jsonPath("$.queue.name", is(message.getQueue().getName())))
                .andExpect(header().string("ETag", etag(message)));
    }

    private String etag(Message message){
        return "\""+message.getMessageId().toString()+"\"";
    }
}
