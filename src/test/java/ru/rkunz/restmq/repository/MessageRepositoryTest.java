package ru.rkunz.restmq.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.rkunz.restmq.model.Message;
import ru.rkunz.restmq.model.Queue;
import static org.junit.Assert.*;

/**
 * Created by rkunz on 01.04.2017.
 */
@RunWith(SpringRunner.class)
@EnableJpaAuditing
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@Transactional
public class MessageRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MessageRepository messageRepository;

    @Before
    @Rollback(false)
    public void setUp(){
        Queue testQueue = new Queue("test");
        this.entityManager.persist(testQueue);

        Queue privateQueue = new Queue("private");
        this.entityManager.persist(privateQueue);

        this.entityManager.persist(new Message("Ubuntu", testQueue));
        this.entityManager.persist(new Message("Debian", testQueue));
        this.entityManager.persist(new Message("Mint", testQueue));
        this.entityManager.persist(new Message("OpenSuse", testQueue));
        this.entityManager.persist(new Message("Windows", privateQueue));
        this.entityManager.persist(new Message("Fedora", testQueue));
    }

    @Test
    public void testCountByQueue(){
        System.out.println("testCountByQueue");

        long testQueueSize = this.messageRepository.countByQueue(new Queue("test"));
        assertEquals(5, testQueueSize);

        long privateQueueSize = this.messageRepository.countByQueue(new Queue("private"));
        assertEquals(1, privateQueueSize);
    }

    @Test
    public void testFindFirstByQueueOrderByCreated(){
        System.out.println("testFindFirstByQueueOrderByCreated");

        String[] contents = {
                "Ubuntu",
                "Debian",
                "Mint",
                "OpenSuse",
                "Fedora"
        };

        for(int i = 0; i < 5; i++){
            Message nextMessage = this.messageRepository.findFirstByQueueOrderByCreated(new Queue("test"));
            System.out.println("Next message: " + nextMessage);
            assertEquals(contents[i], nextMessage.getContent());
            this.messageRepository.delete(nextMessage);
        }
    }
}
