package ru.rkunz.restmq.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.rkunz.restmq.configuration.ServiceTestConfiguration;
import ru.rkunz.restmq.exception.QueueResourceAlreadyExistsException;
import ru.rkunz.restmq.exception.QueueResourceNotFoundException;
import ru.rkunz.restmq.model.Queue;
import ru.rkunz.restmq.repository.MessageRepository;
import ru.rkunz.restmq.repository.QueueRepository;
import static org.junit.Assert.*;

import static org.mockito.BDDMockito.given;

/**
 * Created by rkunz on 01.04.2017.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = QueueServiceImpl.class)
public class QueueServiceTest {

    private static final String QUEUE_NAME = "test";

    @Autowired
    private QueueService queueService;

    @MockBean
    private QueueRepository queueRepository;

    @MockBean
    private MessageRepository messageRepository;

    @Test(expected = QueueResourceNotFoundException.class)
    public void getNextMessageShouldThrowQueueResourceNotFoundException(){
        System.out.println("getNextMessageShouldThrowQueueResourceNotFoundException");

        given(this.queueRepository.findOne(QUEUE_NAME)).willReturn(null);

        this.queueService.getQueue(QUEUE_NAME);
    }

    @Test
    public void getNextMessageShouldReturnQueue(){
        System.out.println("getNextMessageShouldReturnQueue");

        Queue expectedQueue = new Queue(QUEUE_NAME);

        given(this.queueRepository.findOne(QUEUE_NAME)).willReturn(expectedQueue);

        Queue returnedQueue = this.queueService.getQueue(QUEUE_NAME);
        assertEquals(expectedQueue, returnedQueue);
    }

    @Test(expected = QueueResourceAlreadyExistsException.class)
    public void createShouldThrowQueueResourceAlreadyExistsException(){
        System.out.println("createShouldThrowQueueResourceAlreadyExistsException");

        Queue queue = new Queue(QUEUE_NAME);
        given(this.queueRepository.exists(QUEUE_NAME)).willReturn(true);

        this.queueService.create(queue);
    }

    @Test
    public void createShouldReturnQueue(){
        System.out.println("createShouldReturnQueue");

        Queue expectedQueue = new Queue(QUEUE_NAME);
        given(this.queueRepository.exists(QUEUE_NAME)).willReturn(false);
        given(this.queueRepository.save(new Queue(QUEUE_NAME))).willReturn(expectedQueue);

        Queue returnedQueue = this.queueService.create(new Queue(QUEUE_NAME));
        assertNotNull(returnedQueue);
        assertEquals(expectedQueue, returnedQueue);
    }

    @Test
    public void getSizeShouldReturnLong(){
        System.out.println("getSizeShouldReturnLong");

        long expectedSize = 10;

        Queue queue = new Queue(QUEUE_NAME);
        given(this.queueRepository.findOne(QUEUE_NAME)).willReturn(queue);
        given(this.messageRepository.countByQueue(queue)).willReturn(expectedSize);

        long returnedSize = this.queueService.getSize(QUEUE_NAME);
        assertEquals(expectedSize, returnedSize);
    }
}
