package ru.rkunz.restmq.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.rkunz.restmq.model.Message;
import ru.rkunz.restmq.model.Queue;
import ru.rkunz.restmq.repository.MessageRepository;
import ru.rkunz.restmq.repository.QueueRepository;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

import static org.mockito.BDDMockito.*;
import static org.junit.Assert.*;

/**
 * Created by rkunz on 01.04.2017.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = MessageServiceImpl.class)
public class MessageServiceTest {

    private static final String QUEUE_NAME = "test";

    @MockBean
    private MessageRepository messageRepository;

    @MockBean
    private QueueService queueService;

    @Autowired
    private MessageService messageService;

    @Test
    public void getNextMessageShouldReturnNull(){
        System.out.println("getNextMessageShouldReturnNull");

        Queue queue = new Queue(QUEUE_NAME);
        given(this.queueService.getQueue(QUEUE_NAME)).willReturn(queue);
        given(this.messageRepository.findFirstByQueueOrderByCreated(queue)).willReturn(null);

        assertNull(this.messageService.getNextMessage(QUEUE_NAME));
    }

    @Test
    public void getNextMessageShouldReturnMessage(){
        System.out.println("getNextMessageShouldReturnMessage");

        Message expectedMessage = new Message("Ubuntu");

        Queue queue = new Queue(QUEUE_NAME);
        given(this.queueService.getQueue(QUEUE_NAME)).willReturn(queue);
        given(this.messageRepository.findFirstByQueueOrderByCreated(queue)).willReturn(expectedMessage);

        Message returnedMessage = this.messageService.getNextMessage(QUEUE_NAME);

        assertEquals(expectedMessage, returnedMessage);
    }

    @Test
    public void createShouldReturnMessage(){
        System.out.println("createShouldReturnMessage");

        Queue queue = new Queue(QUEUE_NAME);
        Queue wrongQueue = new Queue("wrong");

        Message expectedMessage = new Message();
        expectedMessage.setMessageId(UUID.randomUUID());
        expectedMessage.setQueue(queue);
        expectedMessage.setContent("Debian");
        expectedMessage.setCreated(new Date());

        given(this.queueService.getQueue(QUEUE_NAME)).willReturn(queue);
        given(this.messageRepository.save(new Message("Debian", queue))).willReturn(expectedMessage);

        Message returnedMessage
                = this.messageService.create(QUEUE_NAME, new Message("Debian", wrongQueue));
        assertNotNull(returnedMessage);
        assertEquals(expectedMessage, returnedMessage);
    }
}
